import os
from os.path import basename
# Options
from optparse import OptionParser
# Email
from email.utils import formatdate
#from email.mime.image import MIMEImage
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


# VARIABLES
################

# Email server.
emailHost = ''
# Int.
emailPort = 25
# True/False.
emailTLS = False
emailSSL = False
# Email credentials.
emailUser = ''
emailPwd = ''
# Email subject.
emailSubject = ''
# Email From info.
emailFrom = ''
# Email To and CC.
emailTo = ''
emailToCc = ''
# Attachment/s.
emailAttachment = ''
# Replay header.
emailReplyTo = ''


# CLASSES
#######################

# Options parser
def programParser():
      try:
            # Description and version variables.
            usage = "python %prog [options] bodyContentFile.[html|txt]"
            description = "Program to send email with HTML or Plain text content, with also attachments.\n\nConfigure the variables section to connect to your email server, or use the options to change any of these.\n\nSpecify the body content in a .html or .txt file, depending on the format you choose.\n\nNever use single quotes ('') in each option value. If you not add white spaces quotes are not neccesary, but if you use it only double quotes (\"\") are allowed."
            version = '%prog: version 1.0'

            # Parser init.
            parser = OptionParser(usage,version=version,description=description,add_help_option=True)

            # Parser options.
            parser.add_option('-H', '--host', dest = 'host', help = 'SMTP server.')
            parser.add_option('-p', '--port', dest = 'port', help = 'SMTP port.')
            parser.add_option('--tls', action = 'store_true', help = 'TLS security on.')
            parser.add_option('--ssl', action = 'store_true', help = 'SSL security on.')
            parser.add_option('--nosecurity', action = 'store_true', help = 'No TLS or SSL security.')
            parser.add_option('-u', '--username', dest = 'username', help = 'Username login.')
            parser.add_option('-P', '--password', dest = 'password', help = 'Password login.')
            parser.add_option('-s', '--subject', dest = 'subject', help = 'Email subject, between double quotes (\"\").')
            parser.add_option('-f', '--from', dest = 'emailFrom', help = 'Email from. Different than Username could be considered as Email Spoofing. If it is not defined it will be the same than the user name.')
            parser.add_option('-t', '--to', dest = 'emailTo', help = 'Email destination. More than one separated by comas (,), with no white spaces.')
            parser.add_option('-c', '--carboncopy', dest = 'emailToCc', help = 'Email destination copy. More than one separated by comas (,), with no white spaces.')
            parser.add_option('-a', '--attachment', dest = 'emailAttachment', help = 'Attach a file or files, separated by comas (,) and between double quotes (\"\").')
            parser.add_option('-r', '--replyto', dest = 'emailReplyTo', help = 'Email to reply. If it is not defined it will be the email from.')

            return parser

      except Exception as e:
            raise ValueError('Function - creating program parser: %s' % str(e))

# Check parser to change file variables.
def checkParserOptionsArguments(options,args):
      try:
            # ARGUMENTS, only 1 is available. No arguments or more than one show an error.
            if len(args) == 0:
                  raise ValueError('No argument has been indicated.')
            elif len(args) > 1 or len(args) < 0:
                  raise ValueError('Only one argument is allowed.')
            else:
                  # Check the argument extension. Only .html or .txt are available.
                  extension = args[0].split('.')
                  fileExt = extension[1].lstrip('\'').rstrip('\'')
                  
                  # Other extensions than .html or .txt extension show an error.
                  if fileExt != 'html' and fileExt != 'txt':
                        raise ValueError('Only files with extensions .html or .txt are allowed.')
                  else:

                        # Identify email type by the file extension (HTML or Plain text).
                        global emailType
                        if fileExt == 'html':
                              emailType = 'html'
                        elif fileExt == 'txt':
                              emailType = 'plain'

            # Parser options added.
            # Email Host.
            if options.host:
                  global emailHost
                  emailHost = str(options.host.lstrip('\'').rstrip('\''))
            
            # Email Port.
            if options.port:
                  global emailPort
                  emailPort = int(options.port.lstrip('\'').rstrip('\''))
            
            # Security, only one or none allowed.
            if options.tls and options.ssl:
                  raise ValueError('Only one option is available, --ssl or --tls, or nothing.')
            elif options.nosecurity:
                  global emailSSL
                  global emailTLS
                  emailTLS = False
                  emailSSL = False                  
            else:                  
                  if options.tls:
                        emailTLS = True
                        emailSSL = False
                  if options.ssl:
                        emailSSL = True
                        emailTLS = False
            
            # Email Username
            if options.username:
                  global emailUser
                  emailUser = str(options.username.lstrip('\'').rstrip('\''))

            # Email password
            if options.password:
                  global emailPwd
                  emailPwd = str(options.password.lstrip('\'').rstrip('\''))

            # Email subject.
            if options.subject:
                  global emailSubject
                  emailSubject = str(options.subject.lstrip('\'').rstrip('\''))
            
            # Email from.
            if options.emailFrom:
                  global emailFrom
                  emailFrom = str(options.emailFrom)

            # Email/s to.
            if options.emailTo:
                  global emailTo
                  emailTo = str(options.emailTo.lstrip('\'').rstrip('\''))

            # Email/s in Carbon Copy.
            if options.emailToCc:
                  global emailToCc
                  emailToCc = str(options.emailToCc.lstrip('\'').rstrip('\''))

            # Email attachment/s.
            if options.emailAttachment:
                  global emailAttachment
                  emailAttachment = str(options.emailAttachment.lstrip('\'').rstrip('\''))

            # Email reply header.
            if options.emailReplyTo:
                  global emailReplyTo
                  emailReplyTo = str(options.emailReplyTo.lstrip('\'').rstrip('\''))

      except Exception as e:
            raise ValueError('Function - checking parser variables: %s' % str(e))
      

# Email Class
class emailClass():

      # Constructor with email config variables.
      def __init__(self,emailHost,emailPort,emailTLS,emailSSL,emailUser,emailPwd,emailSubject,emailFrom,emailTo,emailToCc,emailReplyTo):
            self.emailHost = emailHost
            self.emailPort = emailPort
            self.emailTLS = emailTLS
            self.emailSSL = emailSSL
            self.emailUser = emailUser
            self.emailPwd = emailPwd
            self.emailSubject = emailSubject

            self.emailFrom = emailFrom
            if not self.emailFrom:
                  self.emailFrom = self.emailUser
            elif not '@' in self.emailFrom:
                  self.emailFrom = self.emailFrom + " <" + self.emailUser + ">"
            
            self.emailTo = emailTo
            self.emailToCc = emailToCc
            
            self.emailReplyTo = emailReplyTo
            if not self.emailReplyTo:
                  self.emailReplyTo = self.emailFrom

            self.mailRcpt = self.emailTo.split(',') + self.emailToCc.split(',')

            if self.emailTLS and self.emailSSL:
                  raise ValueError('Check variables for SSL and TLS in the program, only one or nothing are availables.')

      # Send the email from config variables (from file or command options).
      def emailSend(self,type,bodyContent,attach=None):
            try:
                  # Type (HTML or Plain text).
                  if type == 'html':
                        msg = MIMEMultipart('related')
                  elif type == 'plain':
                        msg = MIMEMultipart()
                  else:
                        raise ValueError('Class - Error declaring email type (html or plain).')
                  
                  # Message info.
                  msg['Subject'] = self.emailSubject
                  msg['From'] = self.emailFrom
                  msg['To'] = self.emailTo
                  msg['Cc'] = self.emailToCc
                  msg["Date"] = formatdate(localtime=True)
                  msg["Reply-To"] = self.emailReplyTo

                  # Body content.
                  with open(bodyContent) as f:
                        body = f.read()
                  if type == 'html':
                        part2 = MIMEText(body,'html')
                  elif type == 'plain':
                        part2 = MIMEText(body,'plain')
                  else:
                        raise ValueError('Class - Error declaring email type (html or plain).')
                  msg.attach(part2)

                  # Attach file/s, it it has been indicated.
                  if attach:
                        for f in attach.split(','):                                                   
                              with open(f, "rb") as file:
                                    part = MIMEApplication(file.read(), Name=basename(f))
                              part['Content-Disposition'] = 'attachment; filemane=%s' % basename(f)
                              msg.attach(part)

                  # Create message.
                  textBody = msg.as_string()


                  # Configure security, if it has been configured.
                  if self.emailSSL:
                        connection = smtplib.SMTP_SSL(self.emailHost,self.emailPort)
                        connection.ehlo()
                  else:
                        connection = smtplib.SMTP(self.emailHost,self.emailPort)
                        connection.ehlo()
                        if self.emailTLS:
                              connection.starttls()

                  # Email credentials, if it has been configured.
                  if self.emailUser:
                        connection.login(self.emailUser,self.emailPwd)

                  # Send the email.
                  connection.sendmail(self.emailFrom,self.mailRcpt,textBody)

                  # Close connection.
                  connection.quit()

            except Exception as e:
                  raise ValueError('Class error: %s' % (str(e)))


# PROGRAM
###########

try:

      # Parse argument and options, if exists.
      parser = programParser()
      (options, args) = parser.parse_args()

      # Check parser options and argument.
      checkParserOptionsArguments(options,args)

      email = emailClass(emailHost,emailPort,emailTLS,emailSSL,emailUser,emailPwd,emailSubject,emailFrom,emailTo,emailToCc,emailReplyTo)

      # html or plain / html body file or plain text body, and the attachment if is required.
      email.emailSend(emailType,args[0].lstrip('\'').rstrip('\''),emailAttachment)

      # If no error is shown, print ok.
      print('Email sent!!!')

except Exception as e:
      print('ERROR - %s' % (str(e)))
finally:
      print('\nPowered by https://www.tiraquelibras.com/blog')
