# Introducción

Programa para realizar envíos de correo con contenido en formato **HTML** o **Texto plano**, a partir de un archivo con extensión ***.html*** o ***.txt*** respectivamente.

Dispone de soporte para **SSL/TLS** y **STARTTLS**.

Posibilidad de enviar a uno o varios destinatarios, incluidos en **CC**, y  con cambio de identidad, si el proveedor del servicio de correo lo permite, <u>aunque no recomendamos el uso de esta práctica</u>.

También tenemos la opción de adjuntar uno o varios archivos en el mismo envío.



# Requisitos

El programa está desarrollado para ser usado con **Python 3.x**.



# Instalación

No se requiere la instalación de módulos adicionales, ya que se encuentran incluidos por defecto en la versión del lenguaje en el que se ha desarrollado. Por lo tanto, tampoco será necesario ejecutar en un ***entorno virtual*** de **Python**.

Para clonar el repositorio nos ubicamos en el directorio en donde queramos instalar el programa y usamos el siguiente comando para clonarlo:

```
git clone https://sergiobr@bitbucket.org/sergiobr/smtp-contenido-html-o-texto-plano-desde-python.git
```

Y ya estaría listo para su uso en el directorio en donde lo hayamos clonado.



# Configuración



## Dentro del ejecutable

Dentro del archivo **envio.py** tenemos al comienzo la sección **VARIABLES** en donde podemos indicar los datos de acceso al buzón de correo para realizar el envío, incluido/s el/los destinatario/s del mensaje.

```
# Email server.
emailHost = ''
# Int.
emailPort = 25
# True/False.
emailTLS = False
emailSSL = False
# Email credentials.
emailUser = ''
emailPwd = ''
# Email subject.
emailSubject = ''
# Email From info.
emailFrom = ''
# Email To and CC.
emailTo = ''
emailToCc = ''
# Attachment/s.
emailAttachment = ''
# Replay header.
emailReplyTo = ''
```

En esta sección configuramos:

- Servidor de email.
- Puerto del servidor de email.
- Seguridad SSL/TLS o STARTTLS, si la tuviera.
- Credenciales del buzón para realizar el envío.
- Asunto del mensaje.
- Información del origen o From.
- Destinatarios y destinatarios en copia.
- Adjuntos, si los tuviera.
- Cabecera de respuesta.



## Usando las opciones del programa (prioritario)

También podemos indicar estos parámetros en las opciones del programa, **<u><span style='color: red;'>las cuales tienen prioridad sobre lo que se configure en el archivo, comentado justo encima de estas líneas, en la sección [Dentro del ejecutable](#Dentro-del-ejecutable)</span></u>**.

Estas serían las opciones:

```
Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -H HOST, --host=HOST  SMTP server.
  -p PORT, --port=PORT  SMTP port.
  --tls                 TLS security on.
  --ssl                 SSL security on.
  --nosecurity          No TLS or SSL security.
  -u USERNAME, --username=USERNAME
                        Username login.
  -P PASSWORD, --password=PASSWORD
                        Password login.
  -s SUBJECT, --subject=SUBJECT
                        Email subject, between double quotes ("").
  -f EMAILFROM, --from=EMAILFROM
                        Email from. Different than Username could be
                        considered as Email Spoofing. If it is not defined it
                        will be the same than the user name.
  -t EMAILTO, --to=EMAILTO
                        Email destination. More than one separated by comas
                        (,), with no white spaces.
  -c EMAILTOCC, --carboncopy=EMAILTOCC
                        Email destination copy. More than one separated by
                        comas (,), with no white spaces.
  -a EMAILATTACHMENT, --attachment=EMAILATTACHMENT
                        Attach a file or files, separated by comas (,) and
                        between double quotes ("").
  -r EMAILREPLYTO, --replyto=EMAILREPLYTO
                        Email to reply. If it is not defined it will be the
                        email from.
```

El listado de opciones mostradas son las siguientes:

| Opción        | Variante              | Descripción                                                  |
| ------------- | --------------------- | ------------------------------------------------------------ |
|               | --version             | Muestra la versión del programa.                             |
| -h            | --help                | Muestra la ayuda del programa.                               |
| -H <host>     | --host=<host>         | Servidor de correo para realizar el envío.                   |
| -p <port>     | --port=<port>         | Puerto del servidor de correo.                               |
|               | --tls                 | Seguridad STARTTLS.                                          |
|               | --ssl                 | Seguridad SSL.                                               |
|               | --nosecurity          | Anular cualquier tipo de seguridad.                          |
| -u <username> | --username=<username> | Login de usuario.                                            |
| -P <passwd>   | --password=<passwd>   | Contraseña de usuario.                                       |
| -s <subject>  | --subject=<subject>   | Asunto del mensaje.                                          |
| -f <from>     | --from=<from>         | Información del From del mensaje.                            |
| -t <to>       | --to=<to>             | Destinatario/s, separados por comas (,).                     |
| -c <cc>       | --carboncopy=<cc>     | Destinatario/s en copia, separados por comas(,).             |
| -a <file/s>   | --attachment=<file/s> | Archivo/s adjunto/s, separados por comas (,).                |
| -r <reply-to> | --replyto=<reply-to>  | Cabecera de respuesta. Si no se indica coincidirá con la dirección de *From*. |



# Ejecución

Para ejecutar el programa utilizamos el siguiente comando:

```python
pythonX envio.py <argumento> <opciones>
```

Siendo en nuestro caso:

- **pythonX **-> python3
- **\<argumento\>** -> archivo.html o archivo.txt
- **\<opciones\>** -> las opciones que veremos más adelante en la ayuda.

Por ejemplo:

```python
python3 envio.py archivo.html
```

Para consultar las opciones del programa disponemos de una ayuda contextual agregando la opción ***-h*** o ***--help***. **<u><span style='color:red;'>Recordar que estas tienen prioridad sobre las variables configuradas en el mismo ejecutable, comentado en la sección anterior de [Configuración](#Configuración)</span></u>**:

```bash
python3 envio.py archivo.html --help
```

Siendo el resultado:

``` 
Usage: python envio.py [options] bodyContentFile.[html|txt]

Program to send email with HTML or Plain text content, with also attachments.
Configure the variables section to connect to your email server, or use the
options to change any of these.  Specify the body content in a .html or .txt
file, depending on the format you choose.  Never use single quotes ('') in
each option value. If you not add white spaces quotes are not neccesary, but
if you use it only double quotes ("") are allowed.

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -H HOST, --host=HOST  SMTP server.
  -p PORT, --port=PORT  SMTP port.
  --tls                 TLS security on.
  --ssl                 SSL security on.
  --nosecurity          No TLS or SSL security.
  -u USERNAME, --username=USERNAME
                        Username login.
  -P PASSWORD, --password=PASSWORD
                        Password login.
  -s SUBJECT, --subject=SUBJECT
                        Email subject, between double quotes ("").
  -f EMAILFROM, --from=EMAILFROM
                        Email from. Different than Username could be
                        considered as Email Spoofing. If it is not defined it
                        will be the same than the user name.
  -t EMAILTO, --to=EMAILTO
                        Email destination. More than one separated by comas
                        (,), with no white spaces.
  -c EMAILTOCC, --carboncopy=EMAILTOCC
                        Email destination copy. More than one separated by
                        comas (,), with no white spaces.
  -a EMAILATTACHMENT, --attachment=EMAILATTACHMENT
                        Attach a file or files, separated by comas (,) and
                        between double quotes ("").
  -r EMAILREPLYTO, --replyto=EMAILREPLYTO
                        Email to reply. If it is not defined it will be the
                        email from.

Powered by https://www.tiraquelibras.com/blog

```

Indicar que las opciones en formato ***string*** no deben de contener espacios en blanco, o si los contienen como por ejemplo el **asunto** se deben de utilizar comillas dobles (**""**), <u>nunca usar comillas simples (**''**)</u>. El programa está diseñado para sustituirlas, pero puede que algún caso no se hubiera contemplado, dando un error como resultado final. Por ejemplo, si queremos enviar a más de un destinatario podemos indicar las direcciones sin comillas dobles y sin espacios en blanco:

email1@example.org,email2@example.org

O con comilla dobles:

"email1@example.org,email2@example.org"



Para mostrar la versión del programa ejecutamos el comando:

```bash
python envio.py --version
```

Siendo el resultado:

``` bash
>python envio.py --version
envio.py: version 1.0

Powered by https://www.tiraquelibras.com/blog
```



# Ejemplo

Creamos un archivo **prueba.html** con el siguiente contenido:

``` html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Email de prueba</h1>
    <hr>
    <p>Esto es un email de prueba.</p>
    <br>
    <p>Saludos</p>
</body>
</html>
```

Y lo enviamos ejecutando el comando de la siguiente forma, sin usar opciones, únicamente con las variables configuradas en el ejecutable:

```bash
python envio.py prueba.html
```

Siendo el resultado el siguiente:

```bash
>python envio.py prueba.html
Email sent!!!

Powered by https://www.tiraquelibras.com/blog
```

Y en el destino se vería de la siguiente forma:

![ejemplo](https://www.tiraquelibras.com/blog/wp-content/uploads/2020/02/1.jpg)

Lo mismo con un archivo de texto con extensión **.txt** y texto plano en su interior.